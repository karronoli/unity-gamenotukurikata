﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public bool is_landing = false;
	protected float jump_speed = 12.0f;
	// Use this for initialization
	void Start () {
        this.is_landing = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (this.is_landing && Input.GetMouseButtonDown (0)) {
            this.is_landing = false;
			this.rigidbody.velocity = Vector3.up * this.jump_speed;
		}
	}

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Floor") {
            this.is_landing = true;
        }
    }
}
